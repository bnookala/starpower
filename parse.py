#!/usr/bin/env python

import json
import pickle
import os.path
import sys

# This file is only intended to be used when the pickle is not available
if os.path.exists('pickled_reviews'):
    print "Pickled reviews exist!"
    sys.exit()

# The two files that we need, one to pickle review data in, and the other to read from
pickled_reviews_file = open('pickled_reviews', 'wb')
json_file = open('yelp_academic_dataset.json', 'rb')

data = []

# Iterate through every line and check if the object is a review
for line in json_file:
    json_data = json.loads(line)
    if json_data['type'] == 'review':
        data.append(json_data)

pickle.dump(data, pickled_reviews_file)

pickled_reviews_file.close()
json_file.close()
