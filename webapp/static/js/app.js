$(window).load(function() {
    console.log("assets loaded");

    var sentimentAnalysisAPI = "http://text-processing.com/api/sentiment/";

    var submit_form = function () {
        
    };

    var submit_to_text_processing = function (form_text) {
        $.ajax({
            type: 'POST',
            url: sentimentAnalysisAPI,
            data: form_text,
            success: function(data) {
                console.log(data);
            },
        });
    };

    $('a#submit_form').bind('click', function (event) {
        event.stopPropagation();
        event.preventDefault();
        submit_form();
    });
});
