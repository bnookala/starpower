#!/usr/bin/env python

import nltk
import nltk.classify.util
import itertools
import collections
import cPickle as pickle
import os
import random
from nltk.collocations import BigramCollocationFinder, TrigramCollocationFinder
from nltk.metrics import BigramAssocMeasures, TrigramAssocMeasures
from nltk.classify import MaxentClassifier
from nltk.corpus import stopwords
from nltk.corpus.reader import CategorizedPlaintextCorpusReader

# I created two symlinks in the ~/nltk_data/corpora/ directory to the ./yelp_pos_neg and ~/yelp_neutral directory to make this work
yelp_pos_neg_dir = nltk.data.find('corpora/yelp_pos_neg')
neutral_dir = nltk.data.find('corpora/yelp_neutral')

reader_pos_neg = CategorizedPlaintextCorpusReader(yelp_pos_neg_dir, r'(?!\.).*\.txt', cat_pattern=r'(pos|neg)/.*')
reader_neutral = CategorizedPlaintextCorpusReader(neutral_dir, r'(?!\.).*\.txt', cat_pattern=r'(neutral)/.*')

rated_files = [reader_pos_neg.fileids(rating) for rating in ['pos','neg']]
neutral_files = [reader_neutral.fileids(rating) for rating in ['neutral']]

def bag_of_words(words):
    # Create a feature dict of words
    return dict([(word, True) for word in words])

def bag_of_words_not_in_set(words, common):
    # Remove stopwords
    return bag_of_words(set(words) - set(common))

def bag_of_non_stopwords(words, stopfile='english'):
    # Remove all instances of stopwords
    badwords = stopwords.words(stopfile)
    return bag_of_words_not_in_set(words, badwords)

def bag_of_bigram_words(words, bigram_score_fn=BigramAssocMeasures.chi_sq, trigram_score_fn=TrigramAssocMeasures.chi_sq, n=200):
    # Find the top 200 bigrams and top 200 trigrams in a review and use them to build
    bigram_finder = BigramCollocationFinder.from_words(words)
    trigram_finder = TrigramCollocationFinder.from_words(words)
    bigrams = bigram_finder.nbest(bigram_score_fn, n)
    trigrams = trigram_finder.nbest(trigram_score_fn, n)
    toReturn = bag_of_words(words)
    toReturn.update(bag_of_words(bigrams))
    toReturn.update(bag_of_words(trigrams))
    return toReturn

def label_feats_from_corpus(corpus, feature_detector=bag_of_words):
    label_feats = collections.defaultdict(list)
    for label in corpus.categories():
        for fileid in corpus.fileids(categories=[label]):
            feats = feature_detector(corpus.words(fileids=[fileid]))
            label_feats[label].append(feats)
    return label_feats

def split_label_feats(lfeats, split=0.80):
    # Takes a list of features and returns a split of these features, one split to train upon, and the the other to test upon
    train_feats = []
    test_feats = []
    for label, feats in lfeats.iteritems():
        cutoff = int(len(feats) * split)
        train_feats.extend([(feat, label) for feat in feats[:cutoff]])
        test_feats.extend([(feat, label) for feat in feats[cutoff:]])
    return train_feats, test_feats

def start():
    # Start execution of the classifier
    print "Extracting features"
    labeled_features_pos_neg = label_feats_from_corpus(reader_pos_neg, bag_of_bigram_words)
    train_set, test_set = split_label_feats(labeled_features_pos_neg)
    print "Training"

    # Train on the original data
    me_classifier = MaxentClassifier.train(train_set, algorithm='megam', trace=0, max_iter=10)
    print "Testing"
    print "Accuracy", nltk.classify.util.accuracy(me_classifier, test_set)

    print "Classifying neutral reviews"
    labeled_features_neutral = label_feats_from_corpus(reader_neutral, bag_of_bigram_words)
    useful, useless = split_label_feats(labeled_features_neutral, split=1.0)

    # Classify the neutral reviews into pos/neg
    for index, item in enumerate(useful):
        label = me_classifier.classify(item[0])
        labeled_features_pos_neg[label].append(item[0])

    print "Finished reclassifying - training new classifier"
    # Train on the modified data
    train_set_new, test_set_new = split_label_feats(labeled_features_pos_neg)
    new_me_classifier = MaxentClassifier.train(train_set_new, algorithm='megam', trace=0, max_iter=10)
    print "New accuracy", nltk.classify.util.accuracy(new_me_classifier, test_set_new)

    print "Serializing new classifier"
    classifier_file = open("classifiers/me_pos_neg_neutral_10000", 'wb')
    pickle.dump(new_me_classifier, classifier_file)
    classifier_file.close()

if __name__ == "__main__":
    start()
