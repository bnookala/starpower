#!/usr/bin/env python

import pickle
import random
import nltk
import os

reviews = pickle.load(open('pickled_reviews'))
current_directory = os.getcwd()

pos_neg_dict = {'pos': [], 'neg': [], 'neutral': []}

test_directories = {
    'pos': current_directory + '/yelp_pos_neg/pos/',
    'neg': current_directory + '/yelp_pos_neg/neg/',
    'neutral': current_directory + '/yelp_neutral/neutral',
}

review_directories = test_directories

# Sort the reviews, so we have some distinction of positive vs. negative
for review in reviews:
    review_rating = int(review['stars'])
    review_text = review['text'].lower()
    if review_rating <= 2:
	pos_neg_dict['neg'].append(review_text)
    elif review_rating >= 4:
	pos_neg_dict['pos'].append(review_text)
    else:
	pos_neg_dict['neutral'].append(review_text)

# Pos/Neg reviews
for tone in ['pos', 'neg']:
    reviews = pos_neg_dict[tone]
    random.shuffle(reviews)
    reviews = reviews[:10000]

    review_directory = review_directories[tone]
    os.chdir(review_directory)

    index = 0
    review_file = open(str(index) + '.txt', 'wb')
    for review_text in reviews:
        review_file.write(unicode(review_text).encode("utf-8"))
        index = index+1
	review_file.close()
	review_file = open(str(index) + '.txt', 'wb')
    review_file.close()

# Neutral Reviews
reviews = pos_neg_dict['neutral']
random.shuffle(reviews)
reviews = reviews[:10000]

review_directory = review_directories['neutral']
os.chdir(review_directory)

index = 0
review_file = open(str(index) + '.txt', 'wb')
for review_text in reviews:
    review_file.write(unicode(review_text).encode("utf-8"))
    index = index+1
    review_file.close()
    review_file = open(str(index) + '.txt', 'wb')
review_file.close()
